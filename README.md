Abel Deployment
===========================

Abel Deployment is a collection of tools to help Stijlbreuk scaffold new Abel projects or edit existing ones.It's based on Chef du Web's [Cutlery]() and [Varying Vagrant Vagrants](https://github.com/Varying-Vagrant-Vagrants/VVV)
---

## Requirements

| Prerequisite    | How to install
| --------------- | ------------- |
| Virtualbox 5.x | [install instructions](https://www.virtualbox.org/)|
| Vagrant | [install instructions](https://www.vagrantup.com/downloads.html) | 
| VVV | [install instructions](https://varyingvagrantvagrants.org/docs/en-US/installation/) |
| WP CLI | [install instructions](http://wp-cli.org/) |
| Gulp | [install instructions](http://gulpjs.com/) |

---

## Installation

* Clone abel-deployment into a folder.
```$ git clone https://bitbucket.org:/abelnl/abel-deployment.git```

* Copy the contents of the cutlery folder to /usr/local/bin
```$ sudo mv abel-deployment/. /usr/local/bin```

* Edit the config-file to point to your VVV installation and fill in some other defaults
```$ sudo nano /usr/local/bin/abel-deployment/config.cfg```

* Edit the users-file to setup all default user accounts the script will create:
```$ sudo nano /usr/local/bin/abel-deployment/users.cfg```

* You should now be able to easily run the abel command from anywhere in your system.

---

## Usage

Once installed, you can run the ```abel``` command anywhere you like. 
Use ```abel --help``` to get a full index on all commands. Here's a quick overview with an example:

```
abel create veenstra							//create a new veenstra site

abel fetch veenstra staging						//fetches the veenstra site from staging for local use

abel fetch veenstra staging --source=demo2		//fetches the demo2 site from staging, renames it veenstra

abel deploy veenstra staging					//deploy veenstra to staging

abel update veenstra							//updates all plugin and dependencies

abel delete veenstra							//deletes the local veenstra reference
```


### Creating a new site

With the command ```abel create {{sitename}}``` you start a new project. Once the script starts running it sets up a few things for you:

- A fresh (single) install of WordPress on VVV
- All must-use plugins for an Abel site. Like the Abel client plugin, Cuisine and Chef Sections.
- A copy of the abel-website-platform parent-theme
- An empty child theme connected to the abel-website-platform parent theme.
- A database, setup with all users specified in /usr/local/bin/abel-deployment/defautls/users.cfg


### Editting an existing site

With the command ```abel fetch {{sitename}} {{environment}}``` you can fetch an existing site from a WordPress installation (both single and multisite) from the specified environment (staging or production). It will create a new entity in VVV and pull in the database and all files needed.


### Deploying a local site

With the command ```abel deploy {{sitename}} {{environment}}``` you can deploy a single local install to the specified environment (staging or production). If the remote environment is a multisite it will create a new sub-site with the given {{sitename}}, it will upload the database, push all media to it's custom subsite folder and push the child-theme to the multisite installation. If it isn't a multisite, the deploy-command will just copy everything as-is and change the main url to the new environment.