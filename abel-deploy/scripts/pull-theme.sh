DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi



git ls-remote "$THEME_SERVER$NAME.git" &>-
if [ "$?" -ne 0 ]; then
    echo "Unable to read from '$THEME_SERVER$NAME.git', going to look for an environment to pull from..."

	if [[ -z "$2" ]]; then
	printf "From which environment are we pulling? "
		read ENV
	else
		ENV=$2
	fi

	bash $DIR/scripts/clone-theme.sh $NAME $ENV $NAME

else

	bash $DIR/scripts/clone-theme.sh $NAME git $NAME
	
fi


echo -e "\033[32m Success! Theme pulled."
echo -e "\033[0m"
exit