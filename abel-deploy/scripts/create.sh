DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

bash $DIR/scripts/new.sh $NAME


echo -e "\033[32m Adding Config files "
echo -e "\033[0m"

cd ~
cd $VVV_FOLDER
cd $NAME/$LOCAL_FOLDER/wp-content/plugins/chef-deploy/Configs

cat $DIR/defaults/staging.env |
sed "s#{{NAME}}#"$NAME"#" > staging.env

cat $DIR/defaults/production.env |
sed "s#{{NAME}}#"$NAME"#" > production.env


cd ~
cd $VVV_FOLDER

if [[ ! $* == *--surpress-vagrant* ]]; then
	bash $DIR/scripts/vagrantreset.sh
fi

echo "$NAME created."
exit