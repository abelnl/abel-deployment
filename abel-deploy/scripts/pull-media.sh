DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

if [[ -z "$2" ]]; then
	printf "From which environment are we pulling? "
	read ENV
else
	ENV=$2
fi

# GET THE TARGET:
if [[ -z "$3" ]]; then
    TARGET_NAME=$NAME
else
	TARGET_NAME=$3
fi

cd ~
cd $VVV_FOLDER/$NAME/
cd wp-content/
rm -rf uploads
mkdir uploads


source ~/$VVV_FOLDER/$NAME/wp-content/plugins/chef-deploy/Configs/$ENV.env



TARGET=$(curl -k $URL/blogId?sitepath=$TARGET_NAME)


# Add the port:
if [[ -z "$REMOTE_PORT" ]] || [[ $REMOTE_PORT == '' ]]; then
    echo 'NO REMOTE PORT SET, GOING WITH DEFAULT'
    PORT_SSH=()
else
    PORT_SSH=(-e "ssh -p $REMOTE_PORT")
fi


if [[ -z "$MULTISITE" ]] || [[ $MULTISITE == false ]]; then

	echo -e "\e[31mYou can't clone from a remote site that isn't a multisite"

else

	echo -e "\033[32m Pulling the media"
	echo -e "\033[0m"


	rsync -av "${PORT_SSH[@]}" $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/wp-content/uploads/sites/$TARGET/ uploads/
    
fi

exit

