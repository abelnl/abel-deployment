DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

if [[ -z "$2" ]]; then
	printf "To which environment are we deploying? "
	read ENV
else
	ENV=$2
fi

# GET THE TARGET:
if [[ -z "$3" ]]; then
    TARGET_NAME=$NAME
else
	TARGET_NAME=$3
fi

# Add the port:
if [[ -z "$REMOTE_PORT" ]] || [[ $REMOTE_PORT == '' ]]; then
	echo 'NO REMOTE PORT SET, GOING WITH DEFAULT'
	PORT_SSH=()
else
	PORT_SSH=(-e "ssh -p $REMOTE_PORT")
fi


cd ~
cd $VVV_FOLDER"/"$NAME"/wp-content/"
source ~/$VVV_FOLDER/$NAME/wp-content/plugins/chef-deploy/Configs/$ENV.env


SITE=$(curl -k $URL/blogId?sitepath=$TARGET_NAME)


echo -e "\033[32m Pushing media"
echo -e "\033[0m"

rsync -av "${PORT_SSH[@]}" uploads/ $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/wp-content/uploads/sites/$SITE --delete --chmod=u=rwx,g=rx,o=rx

exit