DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

if [[ -z "$2" ]]; then
	printf "From which environment are we pulling? "
	read ENV
else
	ENV=$2
fi

if [[ -z "$3" ]]; then
	printf "What's the name of the target site we're cloning into?"
	read TARGET
else
	TARGET=$3
fi


cd ~
cd $VVV_FOLDER/$TARGET
cd wp-content/themes/
rm -rf $TARGET

if [[ $ENV == 'git' ]]; then

	git clone $THEME_SERVER$NAME.git $TARGET

else 

	source ~/$VVV_FOLDER/$TARGET/wp-content/plugins/chef-deploy/Configs/$ENV.env


	# Add the port:
	if [[ -z "$REMOTE_PORT" ]] || [[ $REMOTE_PORT == '' ]]; then
		echo 'NO REMOTE PORT SET, GOING WITH DEFAULT'
		PORT_SSH=()
	else
		PORT_SSH=(-e "ssh -p $REMOTE_PORT")
	fi


	if [[ -z "$MULTISITE" ]] || [[ $MULTISITE == false ]]; then

	echo -e "\e[31mYou can't clone from a remote site that isn't a multisite"

	else

	echo -e "\033[32m Cloning the theme"
	echo -e "\033[0m"


	rsync -av "${PORT_SSH[@]}" $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/wp-content/themes/$NAME/ $TARGET

	fi
fi

exit