DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

cd ~
cd $VVV_FOLDER"/"$NAME"/"$LOCAL_FOLDER"/"

echo -e "\033[32m Cleaning up database"
echo -e "\033[0m"

vagrant ssh --command "cd ../../srv/www/$NAME && wp fetch cleanup $NAME $LOCAL_TOP_DOMAIN"

exit
