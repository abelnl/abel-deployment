DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

if [[ -z "$2" ]]; then
	printf "From which environment are we pulling? "
	read ENV
else
	ENV=$2
fi

cd ~
cd $VVV_FOLDER/$NAME/$LOCAL_FOLDER

source ~/$VVV_FOLDER/$NAME/$LOCAL_FOLDER/wp-content/plugins/chef-deploy/Configs/$ENV.env


# Add the port:
if [[ -z "$REMOTE_PORT" ]] || [[ $REMOTE_PORT == '' ]]; then
	echo 'NO REMOTE PORT SET, GOING WITH DEFAULT'
	PORT_SSH=()
else
	PORT_SSH=(-e "ssh -p $REMOTE_PORT")
fi


echo -e "\033[32m Pushing the theme"
echo -e "\033[0m"

rsync -av "${PORT_SSH[@]}" wp-content/themes/$NAME/ $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/wp-content/themes/$NAME --exclude=node_modules --exclude=.sass-cache --exclude=bower --delete --chmod=u=rwx,g=rx,o=rx

echo -e "\033[32m Success! Theme pushed."
echo -e "\033[0m"
exit