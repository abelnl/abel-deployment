DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

cd ~
cd $VVV_FOLDER"/"$NAME

vagrant ssh --command "cd ../../srv/www/$NAME && wp regenerate sections"
