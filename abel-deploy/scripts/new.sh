DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
NAME=$1

cd ~
cd $VVV_FOLDER

#Make the folder in VVV:
if [ ! -d $NAME ]
then
	mkdir $NAME
fi

cd $NAME
#mkdir $LOCAL_FOLDER
#cd $LOCAL_FOLDER

echo -e "\033[32m Generating gitignore... "
echo -e "\033[0m"

cat $DIR/defaults/gitignore.txt |
sed -e "s#{{NAME}}#"$NAME"#g" > ".gitignore"


echo -e "\033[32m Copying vagrant files... "
echo -e "\033[0m"

sudo cat $DIR/defaults/vvv-init.sh | 
sed -e "s#{{NAME}}#"$NAME"#g" |
sed -e "s#{{TOPDOMAIN}}#"$LOCAL_TOP_DOMAIN"#g" |
sed -e "s#{{LOCAL_FOLDER}}#"$LOCAL_FOLDER"#g" |
sed -e "s#{{ADMIN_NAME}}#"$ADMIN_NAME"#g" |
sed -e "s#{{ADMIN_EMAIL}}#"$ADMIN_EMAIL"#g" |
sed -e "s#{{ADMIN_PW}}#"$ADMIN_PW"#g" |
sed -e "s#{{WPMDB_LICENSE}}#"$WPMDB_LICENSE"#g" |
sed -e "s#{{GF_LICENSE}}#"$GF_LICENSE"#g" |
sed -e "s#{{CHILD_THEME_REPO}}#"$CHILD_THEME_REPO"#g" > "vvv-init.sh"
sudo chown 777 vvv-init.sh


echo -e "\033[32m Copying Composer & WP CLI files "
echo -e "\033[0m"

sudo cat $DIR/defaults/composer.json |
sed -e "s#{{NAME}}#"$NAME"#g" |
sed -e "s#{{TOPDOMAIN}}#"$LOCAL_TOP_DOMAIN"#" |
sed -e "s#{{LOCAL_FOLDER}}#"$LOCAL_FOLDER"#" |
sed -e "s#{{LICENSE}}#"$WPMDB_LICENSE"#g" > "composer.json"
sudo chown 777 composer.json

#vagrant ssh --command "cd ../../srv/www/$NAME && bash vvv-init.sh"

#echo -e "\033[32m Setting Gulp file "
#echo -e "\033[0m"

#cd ~
#cd $VVV_FOLDER
#cd $NAME/$LOCAL_FOLDER/wp-content/themes/$NAME/dev

#cat $DIR/defaults/gulpfile.babel.js |
#sed "s#{{TOPDOMAIN}}#"$LOCAL_TOP_DOMAIN"#g" |
#sed "s#{{NAME}}#"$NAME"#" > gulpfile.babel.js
