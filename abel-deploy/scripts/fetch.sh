DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg

if [[ -z "$1" ]]; then
	printf "What's the name of the site?"
	read NAME
else
	NAME=$1
fi


if [[ -z "$2" ]]; then
	printf "From which environment are we fetching? "
	read ENV
else
	ENV=$2
fi

if [[ $* == *--pause* ]]; then
    bash $DIR/scripts/clone.sh $NAME $ENV $NAME --pause
else
    bash $DIR/scripts/clone.sh $NAME $ENV $NAME
fi