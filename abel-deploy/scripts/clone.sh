DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg

if [[ -z "$1" ]]; then
	printf "What's the name of the site we're cloning?"
	read NAME
else
	NAME=$1
fi


if [[ -z "$2" ]]; then
	printf "From which environment are we are cloning from? "
	read ENV
else
	ENV=$2
fi

if [[ -z "$3" ]]; then
	printf "What's the name of the target site we're cloning into?"
	read TARGET
else
	TARGET=$3
fi

echo -e "\033[32m Cloning $NAME into $TARGET"
echo -e "\033[0m"

cd ~
cd $VVV_FOLDER

if [ -d "$TARGET" ]; then

    bash $DIR/scripts/pull-content.sh $TARGET $ENV
    bash $DIR/scripts/pull-theme.sh $TARGET $ENV

else
    bash $DIR/scripts/new.sh $TARGET


    echo -e "\033[32m Setting up config-files"
    echo -e "\033[0m"


    cd ~
    cd $VVV_FOLDER
    cd $TARGET/$LOCAL_FOLDER/wp-content/plugins/chef-deploy/Configs

    cat $DIR/defaults/staging.env |
    sed "s#{{NAME}}#"$NAME"#" > staging.env

    cat $DIR/defaults/production.env |
    sed "s#{{NAME}}#"$NAME"#" > production.env

   cd ~
    cd $VVV_FOLDER"/"$TARGET"/"$LOCAL_FOLDER"/"

    #pull the database:
    if [[ $* == *--pause* ]]; then
        vagrant ssh --command "cd /srv/www/$TARGET && wp deploy createProfiles && wp fetch $ENV --pause" --quiet
        vagrant ssh --command "cd /srv/www/$TARGET && wp deploy createProfiles && wp fetch $ENV --pause"
    else
        vagrant ssh --command "cd /srv/www/$TARGET && wp deploy createProfiles && wp fetch $ENV"  --quiet
        vagrant ssh --command "cd /srv/www/$TARGET && wp deploy createProfiles && wp fetch $ENV"
    fi

    source ~/$VVV_FOLDER/$TARGET/$LOCAL_FOLDER/wp-content/plugins/chef-deploy/Configs/$ENV.env

    # Add the port:
    if [[ -z "$REMOTE_PORT" ]] || [[ $REMOTE_PORT == '' ]]; then
        echo 'NO REMOTE PORT SET, GOING WITH DEFAULT'
        PORT_SSH=()
    else
        PORT_SSH=(-e "ssh -p $REMOTE_PORT")
    fi


    if [[ -z "$MULTISITE" ]] || [[ $MULTISITE == false ]]; then

    echo -e "\e[31mYou can't clone from a remote site that isn't a multisite"

    else

    echo -e "\033[32m Adding the theme"
    echo -e "\033[0m"

    rm -rf wp-content/themes/$TARGET
    rsync -av "${PORT_SSH[@]}" $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/wp-content/themes/$NAME/ wp-content/themes/$TARGET

    echo -e "\033[32m Deploying the database & media"
    echo -e "\033[0m"

    fi



    cd ~
    cd $VVV_FOLDER
    cd $TARGET/$LOCAL_FOLDER/wp-content/plugins/chef-deploy/Configs

    rm staging.env
    rm production.env

    cat $DIR/defaults/staging.env |
    sed "s#{{NAME}}#"$TARGET"#" > staging.env

    cat $DIR/defaults/production.env |
    sed "s#{{NAME}}#"$TARGET"#" > production.env


    cd ~
    cd $VVV_FOLDER

    vagrant ssh --command "cd /srv/www/$TARGET/$LOCAL_FOLDER/wp-content chown -R www-data uploads && cd ../ && wp deploy createProfiles && wp role reset --all && wp theme activate $TARGET && wp option update blogname $TARGET"


    bash $DIR/scripts/pull-media.sh $TARGET $ENV $NAME; 


    if [[ ! $* == *--surpress-vagrant* ]]; then
        bash $DIR/scripts/vagrantreset.sh
    fi

    echo "$NAME successfully cloned to $TARGET."

 
fi
exit