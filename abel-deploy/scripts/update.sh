DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

cd ~
cd $VVV_FOLDER"/"$NAME
cd $LOCAL_FOLDER"/wp-content/plugins/abel"
git pull origin master
cd ../../themes/abel-website-platform
git pull origin master

cd ../../
cd plugins/chef-deploy/Configs

cat $DIR/defaults/staging.env |
sed "s#{{NAME}}#"$NAME"#" > staging.env

cat $DIR/defaults/production.env |
sed "s#{{NAME}}#"$NAME"#" > production.env

cd ~
cd $VVV_FOLDER"/"$NAME

sudo rm composer.json
sudo cat $DIR/defaults/composer.json |
sed -e "s#{{NAME}}#"$NAME"#g" |
sed -e "s#{{TOPDOMAIN}}#"$LOCAL_TOP_DOMAIN"#" |
sed -e "s#{{LOCAL_FOLDER}}#"$LOCAL_FOLDER"#" |
sed -e "s#{{LICENSE}}#"$WPMDB_LICENSE"#g" > "composer.json"
sudo chown 777 composer.json

vagrant ssh --command "cd ../../srv/www/$NAME && composer update && wp deploy createProfiles"
