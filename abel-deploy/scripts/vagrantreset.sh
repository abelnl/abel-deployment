
echo -e "\033[32m Halting Vagrant... "
echo -e "\033[0m"
vagrant halt

echo -e "\033[32m Upping vagrant... "
echo -e "\033[0m"
vagrant up --provision
