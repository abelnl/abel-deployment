DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

if [[ -z "$2" ]]; then
	printf "To which environment are we pushing? "
	read ENV
else
	ENV=$2
fi

cd ~
cd $VVV_FOLDER"/"$NAME"/"

## RESET THE ENVIRONMENT FILES:
cd wp-content/plugins/chef-deploy/Configs

cat $DIR/defaults/staging.env |
sed "s#{{NAME}}#"$NAME"#" > staging.env

cat $DIR/defaults/production.env |
sed "s#{{NAME}}#"$NAME"#" > production.env

cd ../../../../

echo -e "\033[32m Pushing the database & media"
echo -e "\033[0m"

if [[ $* == *--unpause* ]]; then
    wp deploy $ENV --unpause=true
else
    wp deploy $ENV
fi

exit