if [[ -z "$1" ]]; then
	printf "To which environment are we updating? "
	read ENV
else
	ENV=$1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/defaults/$ENV.env


ssh $REMOTE_USER@$REMOTE_SERVER << EOF
    cd $REMOTE_PATH 
    composer update
    exit
EOF