DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg
#set -x

if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

if [[ -z "$2" ]]; then
	printf "To which environment are we deploying? "
	read ENV
else
	ENV=$2
fi

cd ~
cd $VVV_FOLDER"/"$NAME"/"$LOCAL_FOLDER"/"

source ~/$VVV_FOLDER/$NAME/$LOCAL_FOLDER/wp-content/plugins/chef-deploy/Configs/$ENV.env


# Add the port:
if [[ -z "$REMOTE_PORT" ]] || [[ $REMOTE_PORT == '' ]]; then
	echo 'NO REMOTE PORT SET, GOING WITH DEFAULT'
	PORT_SSH=()
else
	PORT_SSH=(-e "ssh -p $REMOTE_PORT")
fi


if [[ -z "$MULTISITE" ]] || [[ $MULTISITE == false ]]; then

echo -e "\033[32m Deploying to a single install"
echo -e "\033[0m"

cd ../
rsync -av "${PORT_SSH[@]}" $LOCAL_FOLDER/ $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/$NAME --exclude 'wp-config.php' --chmod=u=rwx,g=rx,o=r


else

echo -e "\033[32m Deploying to a multisite-install"
echo -e "\033[0m"

echo -e "\033[32m Adding the theme"
echo -e "\033[0m"

rsync -av "${PORT_SSH[@]}" wp-content/themes/$NAME/ $REMOTE_USER@$REMOTE_SERVER:$REMOTE_PATH/wp-content/themes/$NAME --exclude=node_modules --exclude=.sass-cache --exclude=bower --delete --chmod=u=rwx,g=rx,o=rx

echo -e "\033[32m Deploying the database & media"
echo -e "\033[0m"

fi


if [[ $* == *--unpause* ]]; then
    vagrant ssh --command "cd ../../srv/www/$NAME && wp deploy $ENV --unpause=true"
else
    vagrant ssh --command "cd ../../srv/www/$NAME && wp deploy $ENV"
fi

echo -e "\033[32m Adding admins"
echo -e "\033[0m"

$(curl -k $URL/addAdmins?sitepath=$NAME)


echo -e "\033[32m Site deployed."
echo -e "\033[0m"

exit