DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
source $DIR/config.cfg


if [[ -z "$1" ]]; then
	printf "What's the name of the site? "
	read NAME
else
	NAME=$1
fi

printf "Are you sure you want to delete $NAME? (y/n)" 
read -r continue_delete

if [ "$continue_delete" = 'y' ]; then

	cd ~
	cd $VVV_FOLDER

	vagrant ssh --command "mysql -u root -e 'DROP DATABASE \`$NAME\`;'"
	
	if [[ ! $* == *--surpress-vagrant* ]]; then
		vagrant halt
	fi
	
	rm -rf $NAME

	cd ../
	rm "config/nginx-config/sites/$NAME.conf"
	rm -rf "database/backups/$NAME.sql"
	rm -rf "www/$NAME"

	if [[ ! $* == *--surpress-vagrant* ]]; then
		vagrant up --provision
	fi

	echo "Site deleted."
	exit

elif [ "$continue_delete" = 'n' ]; then
	echo "Site teardown aborted."
	exit
else
	echo "Answer y or n."
	unset continue_delete
fi

exit