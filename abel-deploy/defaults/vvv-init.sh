echo -e "\033[32m Creating database (if it's not there) "
echo -e "\033[0m"

mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS {{NAME}}"
mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON {{NAME}}.* TO wp@localhost IDENTIFIED BY 'wp';"


if [ ! -d wp-includes ]
then
	
	echo -e "\033[32m Installing WordPress... "
	echo -e "\033[0m"
	
	wp core download
	wp core config --dbname="{{NAME}}" --dbuser=root --dbpass=root --dbhost="localhost" --dbprefix="{{NAME}}_" --extra-php="
	define( 'WP_DEBUG', true );
	define( 'WP_DEBUG_LOG', true );
	define( 'WPMDB_LICENCE', '{{WPMDB_LICENSE}}' );
	define( 'GF_LICENSE_KEY', '{{GF_LICENSE}}' );"

	wp core install --url="{{NAME}}{{TOPDOMAIN}}" --title={{NAME}} --admin_user="{{ADMIN_NAME}}" --admin_password="{{ADMIN_PW}}" --admin_email="{{ADMIN_EMAIL}}"
fi


echo -e "\033[32m Plugins & Basisthema installeren... "
echo -e "\033[0m"

composer install

#Install the client plugin:
cd wp-content/plugins
git clone git@bitbucket.org:abelnl/abel-plugin.git abel
cd ../../


# Install a child theme and set some defaults:
if [ ! -d wp-content/themes/{{NAME}} ]
then

	echo -e "\033[32m Installing child theme"
	echo -e "\033[0m"

	cd wp-content/themes
	git clone {{CHILD_THEME_REPO}}.git {{NAME}}
	rm -rf {{NAME}}/.git

	echo -e "\033[32m Removing default themes"
	echo -e "\033[0m"

	rm -rf twentyseventeen
	rm -rf twentyeighteen
	rm -rf twentynineteen
	rm -rf twentytwenty
	rm -rf twentytwentyone
	rm -rf twentysixteen
	rm -rf twentyfifteen

	cd ../../

	wp plugin activate cuisine

	wp cuisine migrate

	wp plugin activate chef-sections
	wp plugin activate chef-deploy
	wp plugin activate abel
	wp plugin activate chef-section-slider
	wp plugin activate chef-section-tabs
	wp plugin activate chef-handpicked-collection-column

	wp plugin activate gravityforms
	wp plugin activate wp-migrate-db-pro
	wp plugin activate wp-migrate-db-pro-cli
	wp plugin activate wp-migrate-db-pro-multisite-tools
	wp plugin activate chef-migrate-media-files

	wp plugin activate post-types-order
	wp plugin activate wordpress-seo
	wp plugin activate post-duplicator
    wp plugin activate adminimize
    wp plugin activate wp-email-smtp

    wp abel setup

	wp theme activate {{NAME}}

	wp user create LucP hi@lucp.nl --role=administrator --user_pass=xepdoor24

	wp post update 2 --post_name=home --post_title=Home
	wp option update page_on_front '2'
	wp option update show_on_front 'page'
	wp option update blog_public '0'
    wp rewrite structure '/%postname%/'
    
fi
